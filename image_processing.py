
def all_int(a_list):
    """
    (list<list<>>) -> bool
    Checks whether a nested list only contains integers.
    >>> all_int([['a','b',1],[2]])
    False
    >>> all_int([[1,2,3],[4]])
    True
    >>> all_int([['a',1],[2]])
    False
    """
    for row in a_list:
        for column in row:
            if type(column) != int:
                return False
    return True

def all_str(a_list):
    """
    (list<list<>>) -> bool
    Checks whether a nested list only contains strings.
    >>> all_str([['a','b','h'],['q','fhg']])
    True
    >>> all_str([[1,2,3,4,5],[4]])
    False
    >>> all_str([['abcd','AxB'],['CxD']])
    True
    """
    for row in a_list:
        for column in row:
            if type(column) != str:
                return False
    return True

def sum_bs_row(row):
    """(list<str>) -> int
    Calculates the sum of the b values in the input row of a compressed image matrix.
    Returns the sum as an int.
    >>> sum_bs_row(["0x5", "200x10"])
    15
    >>> sum_bs_row(["1x2", "200x3","24x4"])
    9
    >>> sum_bs_row(["0x3", "110x7","13x4","15x2"])
    16
    """

    sum_b = 0
    for column in range(len(row)):
        x_index = row[column].find("x")
        b = row[column][x_index+1:len(row[column])]
        b = int(b)
        sum_b = sum_b +b
    return sum_b

def validate_regular_row(row,row_length,file_validation): 
    """
    (list<>,int,bool) -> NoneType
    Validates the row of a regular non-compressed PGM image.
    If the file_validation boolean is true, a string row that contains integers will be casted
    to integers. Each row length will be compared to check they match.
    
    >>> validate_regular_row(["1","2" ,"-4"],3,True)
    Traceback (most recent call last):
    AssertionError: Each element of the image matrix must be greater than 0 and less than 255.
    >>> validate_regular_row(["1", "2", "4"],3,True)
    
    >>> validate_regular_row(["a","b","3"],3,True)
    Traceback (most recent call last):
    AssertionError: The image matrix must contain only integers.
    """
    # Check if it is a nested list.
    if type(row) != list:
        error_message = "The image matrix must be a list of int lists."
        raise AssertionError(error_message)
    if len(row) == 0:
        error_message = "The image matrix cannot contain empty rows."
        raise AssertionError(error_message)
    for column in range(len(row)):
        # If the row is loaded from a file, attempt to cast the string row to an int row.
        if (file_validation):
            negative_number = row[column][0] == '-' and len(row[column]) > 0 and row[column][1:].isdecimal()
            if row[column].isdecimal() or negative_number:
                row[column] = int(row[column])
                
        # Check if each row has the same length
        if row_length != len(row):
            error_message = "Each row of the image matrix must have the same length."
            raise AssertionError(error_message)
        if type(row[column]) != int:
            error_message = "The image matrix must contain only integers."
            raise AssertionError(error_message)
        if row[column] < 0 or row[column] > 255:
            error_message = "Each element of the image matrix must be greater than 0 and less than 255."
            raise AssertionError(error_message)
            
    
def is_valid_image(nested_list):
    """( list<list<>>) -> bool
    Determines if the input list is a valid
    non-compressed PGM image matrix.
    >>> is_valid_image([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    True
    >>> is_valid_image([[-11, 2, 256], [-87, 5, -9], [7, 430, 9]])
    False
    >>> is_valid_image([['abc','AxB'], [4, '32X14', 6], ['ABCDEF', 8, 9]])
    False
    """
    if len(nested_list) == 0:
        return False
    
    row_length = len(nested_list[0])

    for row in nested_list:
        if (type(row)) != list:
            return False
        if len(row) == 0:
            return False
        # Check if each row has the same length
        if row_length != len(row):
            return False
        for column in range(len(row)):
            # Check if each element is an integer
            if type(row[column]) != int:
                return False
            # Check if each integer is between 0 and 255.
            if row[column] < 0 or row[column] > 255:
                return False
    return True


def validate_regular_image(nested_list):
    """
    (list<list<>>) -> NoneType
    Raises an AssertionError with an appropriate error message based on what caused
    the input list to be an invalid non-compressed image matrix.
    
    >>> validate_regular_image([[-11, 2, 256], [-87, 5, -9], [7, 430, 9]])
    Traceback (most recent call last):
    AssertionError: Each element of the image matrix must be greater than 0 and less than 255.
    >>> validate_regular_image([['abc','AxB'], [4, '32X14', 6], ['ABCDEF', 8, 9]])
    Traceback (most recent call last):
    AssertionError: The image matrix must contain only integers.
    >>> validate_regular_image([['abc','AxB'], [4, '32X14'], ['ABCDEF','LMP']])
    Traceback (most recent call last):
    AssertionError: The image matrix must contain only integers.
    """
    if len(nested_list) == 0:
        error_message = "The image matrix cannot be empty."
        raise AssertionError(error_message)
    row_length = len(nested_list[0])

    for row in nested_list:
        validate_regular_row(row,row_length,False) 


def is_valid_compressed_image(nested_list):
    """( list<list<>>) -> bool
    Determines if the input list is a valid
    compressed PGM image matrix.
   
    >>> is_valid_compressed_image([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    False
    >>> is_valid_compressed_image([["3xx6", "114x2"], ["141x8"]])
    False
    >>> is_valid_compressed_image([["12x8", "215x2","4x2"], ["171x12"]])
    True
    """
    if len(nested_list) == 0:
        return False
    
    sum_bs = []
    
    for row in nested_list:
        if (type(row)) != list:
            return False
        if (len(row)) == 0:
            return False
        for column in range(len(row)):
            # Check all elements are strings
            if type(row[column]) != str:
                return False
            x_index = row[column].find("x")
            # Verify that x is present in each column
            if (x_index == -1):
                return False
            a = row[column][0:x_index]
            b = row[column][x_index+1:len(row[column])]
            # Verify that both a and b are positive integers
            if not (a.isdecimal() and b.isdecimal()):
                return False
            a = int(a)
            b = int(b)
        
            if a < 0 or a > 255 or b <= 0:
                return False
            
        sum_bs.append(sum_bs_row(row))

    # Check if the sum of all B values in each row are the same
    for the_sum in sum_bs:
        if the_sum != sum_bs[0]:
            return False
        
    return True

def validate_compressed_row(row,sum_b,row_index):
    """
    (list<>,int,int) -> NoneType
    Raises an appropriate error message based on what's invalid in the input compressed row.
    The sum_b input must be the sum of the b values that the row must have
    and the row_index input is the index of the input row.
    
    >>> validate_compressed_row(["1x4"],4,1)
    
    >>> validate_compressed_row(["1xx6"],6,1)
    Traceback (most recent call last):
    AssertionError: Each pixel must only contain one x.
    >>> validate_compressed_row(["1x4","3"],4,0)
    Traceback (most recent call last):
    AssertionError: Each pixel must contain one x.
    """
    if type(row) != list:
        error_message = "The image matrix must be a list of string lists."
        raise AssertionError(error_message)
    if (len(row)) == 0:
        error_message = "The image matrix cannot contain empty rows."
        raise AssertionError(error_message)
    for column in range(len(row)):
        # Verify each element is a string
        if type(row[column]) != str:
            error_message = "All elements of the image matrix must be strings."
            raise AssertionError(error_message)
        
        x_index = row[column].find("x")
        if (x_index == -1): # Verify that x is present in each column
            error_message = "Each pixel must contain one x."
            raise AssertionError(error_message)
        
        a = row[column][0:x_index]
        b = row[column][x_index+1:len(row[column])]
        # Check if a and b are negative integers
        a_value = a
        b_value = b
        if len(a) > 0:
            if a[0] == '-':
                a_value = a[1:]
        else:
            error_message = "The a value cannot be empty."
            raise AssertionError(error_message)
        if len(b) > 0:
            if b[0] == '-':
                b_value = b[1:]
        else:
            error_message = "The b value cannot be empty."
            raise AssertionError(error_message)
        # Verify that both a and b are integers
        if (a_value.isdecimal() and b_value.isdecimal()):
            a = int(a)
            b = int(b)
        else:
            if('x' in a_value or 'x' in b_value):
                error_message = "Each pixel must only contain one x."
                raise AssertionError(error_message)
            else:
                error_message = "Each pixel in the image matrix must contain an integer before and after the x."
                raise AssertionError(error_message)
  
        if a < 0 or a > 255 or b <= 0:
            error_message = "The a integer must be between 0 and 255 and the b integer must be positive and greater than 0."
            raise AssertionError(error_message)
        
    # Only compare the sum of the bs to rows other than the first row
    if row_index != 0:
        sum_b_row = sum_bs_row(row)
        if sum_b_row != sum_b:
            error_message = "The sum of all b values in each row must be the same."
            raise AssertionError(error_message)

def validate_compressed_image(nested_list):
    """
    (list<list<>>) -> NoneType
    Raises an AssertionError with an appropriate error message based on what makes
    the input image matrix an invalid compressed image matrix.
    
    >>> validate_compressed_image([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    Traceback (most recent call last):
    AssertionError: All elements of the image matrix must be strings.
    >>> validate_compressed_image([["AxB", "CxD"], ["ExF","QxW"], ["PxF", "MxL"]])
    Traceback (most recent call last):
    AssertionError: Each pixel in the image matrix must contain an integer before and after the x.
    >>> validate_compressed_image([["1x4", "2x2"], ["-3x2","13x1"], ["-12x4", "42x2"]])
    Traceback (most recent call last):
    AssertionError: The a integer must be between 0 and 255 and the b integer must be positive and greater than 0.
    """
    if len(nested_list) == 0:
        raise AssertionError("The image matrix cannot be empty.")
    
    first_row = nested_list[0]
    validate_compressed_row(first_row,0,0)
    sum_b_first_row = sum_bs_row(first_row)
    # Validate the rest of the rows and check if the sum of their b values
    # are equal to the sum of the b values in the first row.
    for row_number in range(1,len(nested_list)):
        row = nested_list[row_number]
        row_index = row_number
        validate_compressed_row(row,sum_b_first_row,row_index)
        
            
def validate(image_matrix,compressed):
    """
    (list<list<>>,bool) -> NoneType
    Validates the input image matrix. The compressed boolean determines
    whether the validation is done for a compressed or non-compressed image matrix.
    
    >>> validate([[1,2,3],["a","b","c"]],False)
    Traceback (most recent call last):
    AssertionError: The image matrix must contain only integers.
    >>> validate([["1x3","24x4","33x4"],["12x3","43x1","21x3"]],True)
    Traceback (most recent call last):
    AssertionError: The sum of all b values in each row must be the same.
    >>> validate([["13xx3","2x4","33x4","73x2"],["12x3","43x1","21x3"]],True)
    Traceback (most recent call last):
    AssertionError: Each pixel must only contain one x.
    """
    if compressed:
        validate_compressed_image(image_matrix)
    else:
        validate_regular_image(image_matrix)

    
def validate_three_first_lines(lines,compressed):
    """
    (list<str>,bool) -> NoneType
    Validates the 3 first lines of a PGM image file (compressed and not).
    
    >>> validate_three_first_lines(["P2","34","255"],False)
    Traceback (most recent call last):
    AssertionError: First 3 lines must contain the appropriate file formatting, width and height and maximum white value.
    >>> validate_three_first_lines(["P2C","34 2","255"],True)
    
    >>> validate_three_first_lines(["P2","24 7","255"],False)
    
    """
    formatting = ""
    if compressed:
        formatting = 'P2C'
    else:
        formatting ='P2'
    if len(lines) >= 3:    
        first_line_condition = lines[0] == formatting
        second_line = lines[1].split()
        second_line_condition = len(second_line) == 2 and second_line[0].isdecimal() and second_line[1].isdecimal()
        third_line_condition = lines[2] == '255'
        valid = first_line_condition and second_line_condition and third_line_condition
    else:
        valid = False
    if not valid:
        raise AssertionError("First 3 lines must contain the appropriate file formatting, width and height and maximum white value.")

def load_regular_image(file_name):
    """ (str) -> list<list<int>>
    Returns a PGM image file as an image matrix
    
    >>> load_regular_image("comp.pgm")
    [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 51, 51, 51, 51, 51, 0, 119, 119, 119, 119, 119, 0, 187, 187, 187, 187, 187, 0, 255, 255, 255, 255, 0], [0, 51, 0, 0, 0, 0, 0, 119, 0, 0, 0, 119, 0, 187, 0, 187, 0, 187, 0, 255, 0, 0, 255, 0], [0, 51, 0, 0, 0, 0, 0, 119, 0, 0, 0, 119, 0, 187, 0, 187, 0, 187, 0, 255, 255, 255, 255, 0], [0, 51, 0, 0, 0, 0, 0, 119, 0, 0, 0, 119, 0, 187, 0, 187, 0, 187, 0, 255, 0, 0, 0, 0], [0, 51, 51, 51, 51, 51, 0, 119, 119, 119, 119, 119, 0, 187, 0, 187, 0, 187, 0, 255, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    >>> load_regular_image("abcd")
    Traceback (most recent call last):
    FileNotFoundError: [Errno 2] No such file or directory: 'abcd'
    >>> fobj = open("valid.pgm", "w")
    >>> fobj.write("P2\\n2 2\\n255\\n0 0\\n255 255")
    22
    >>> fobj.close()
    >>> load_regular_image("valid.pgm")
    [[0, 0], [255, 255]]
    """
    image_matrix = []
    file_object = open(file_name, "r")
    # Remove blank lines and separate lines by the \n character
    lines = file_object.read().strip().split("\n")
    validate_three_first_lines(lines[:3],False)
    # Remove first 3 lines
    lines = lines[3:len(lines)]
    first_row = lines[0].split()
    row_length = len(first_row)
    for line_number in range(len(lines)):
        row = lines[line_number].split()
        int_row = []
        # Validate rows as we loop through the file
        validate_regular_row(row,row_length,True)
        
        for x in row:
            # Change from string row to integer row
            int_row.append(int(x))
        image_matrix.append(int_row)
        
       
    file_object.close()
    return image_matrix

def load_compressed_image(file_name):
    """ (str) -> list<list<str>>
    Returns a compressed PGM image file as an image matrix.
    
    >>> load_compressed_image('comp.pgm.compressed')
    [['0x24'], ['0x1', '51x5', '0x1', '119x5', '0x1', '187x5', '0x1', '255x4', '0x1'], ['0x1', '51x1', '0x5', '119x1', '0x3', '119x1', '0x1', '187x1', '0x1', '187x1', '0x1', '187x1', '0x1', '255x1', '0x2', '255x1', '0x1'], ['0x1', '51x1', '0x5', '119x1', '0x3', '119x1', '0x1', '187x1', '0x1', '187x1', '0x1', '187x1', '0x1', '255x4', '0x1'], ['0x1', '51x1', '0x5', '119x1', '0x3', '119x1', '0x1', '187x1', '0x1', '187x1', '0x1', '187x1', '0x1', '255x1', '0x4'], ['0x1', '51x5', '0x1', '119x5', '0x1', '187x1', '0x1', '187x1', '0x1', '187x1', '0x1', '255x1', '0x4'], ['0x24']]
    >>> load_compressed_image('comp')
    Traceback (most recent call last):
    FileNotFoundError: [Errno 2] No such file or directory: 'comp'
    >>> fobj = open("invalid.pgm.compressed", "w")
    >>> fobj.write("P2C\\n30 5\\n255\\nabc3x23 0x0x7\\n")
    27
    >>> fobj.close()
    >>> load_compressed_image("invalid.pgm.compressed")
    Traceback (most recent call last):
    AssertionError: Each pixel in the image matrix must contain an integer before and after the x.
    """
    image_matrix = []
    file_object = open(file_name, "r")
    # Remove blank lines and separate lines by the \n character
    lines = file_object.read().strip().split("\n")
    validate_three_first_lines(lines[:3],True)
    # Remove first 3 lines
    lines = lines[3:len(lines)]
    
    first_row = lines[0].split()
    validate_compressed_row(first_row,0,0)
    sum_b_first_row = sum_bs_row(first_row)
    image_matrix.append(first_row)
    
    for line_number in range(1,(len(lines))):
        row = lines[line_number].split()
        row_index = line_number
        validate_compressed_row(row,sum_b_first_row,row_index)
        image_matrix.append(row)
        
        
    file_object.close()
    return image_matrix

def load_image(file_name):
    """
    (str) -> list<list<>>
    Loads the image file into an image matrix and returns it.
    >>> fobj = open("valid.pgm", "w")
    >>> fobj.write("P2\\n2 2\\n255\\n0 0\\n255 255")
    22
    >>> fobj.close()
    >>> load_image("valid.pgm")
    [[0, 0], [255, 255]]
    >>> fobj = open("invalid.pgm", "w")
    >>> fobj.write("ABCD\\n2 2\\n255\\n0 0\\n255 255")
    24
    >>> fobj.close()
    >>> load_image("invalid.pgm")
    Traceback (most recent call last):
    AssertionError: The first line of the file must be P2 or P2C
   
    >>> load_image('comp.pgm.compressed')
    [['0x24'], ['0x1', '51x5', '0x1', '119x5', '0x1', '187x5', '0x1', '255x4', '0x1'], ['0x1', '51x1', '0x5', '119x1', '0x3', '119x1', '0x1', '187x1', '0x1', '187x1', '0x1', '187x1', '0x1', '255x1', '0x2', '255x1', '0x1'], ['0x1', '51x1', '0x5', '119x1', '0x3', '119x1', '0x1', '187x1', '0x1', '187x1', '0x1', '187x1', '0x1', '255x4', '0x1'], ['0x1', '51x1', '0x5', '119x1', '0x3', '119x1', '0x1', '187x1', '0x1', '187x1', '0x1', '187x1', '0x1', '255x1', '0x4'], ['0x1', '51x5', '0x1', '119x5', '0x1', '187x1', '0x1', '187x1', '0x1', '187x1', '0x1', '255x1', '0x4'], ['0x24']]
    """
    file_object = open(file_name, "r")
    image_data = file_object.read().strip().split("\n")

    first_line = image_data[0]
    first_line = first_line.strip()
    
    image_matrix = []
    if first_line == 'P2':
        image_matrix = load_regular_image(file_name)
    elif first_line == 'P2C':
        image_matrix = load_compressed_image(file_name)
    else:
        raise AssertionError("The first line of the file must be P2 or P2C")
    
    file_object.close()
   
    return image_matrix


def save_regular_image(nested_list,file_name):
    """
    (list<list<>>,str) -> NoneType
    Saves the input list in PGM format to a file with the input filename.
    
    >>> image = [[0]*10, [255]*10, [0]*10]
    >>> save_regular_image(image, "test.pgm")
    >>> image2 = load_image("test.pgm")
    >>> image == image2
    True
    >>> save_regular_image([["8x12"],["2x14"]], "test2.pgm")
    Traceback (most recent call last):
    AssertionError: The image matrix must contain only integers.
    >>> save_regular_image([[-13,12,257],[11,16,13]], "test3.pgm")
    Traceback (most recent call last):
    AssertionError: Each element of the image matrix must be greater than 0 and less than 255.
    """
    validate(nested_list,False)
   
    width = str(len(nested_list))
    height = str(len(nested_list[0]))
    image_format = "P2"
    max_white_value = '255'
    file = open(file_name,'w')
    file.write(image_format+'\n')
    file.write(height + " "+width +'\n')
    file.write(max_white_value+'\n')
   
    column = 0
   
    for row in range(len(nested_list)):
        for item in nested_list[row]:
            file.write(str(item))
            if column != len(nested_list[row]) -1:
                file.write(" ")
            column = column +1
        file.write("\n")
        column = 0
    file.close()


def save_compressed_image(nested_list,file_name):
    """
    (list<list<>>,str) -> NoneType
    Saves the input list in compressed PGM format to a file with the input file name.
    
    >>> image = [["0x5", "200x2"], ["111x7"]]
    >>> save_compressed_image(image, "test.pgm")
    >>> image2 = load_compressed_image("test.pgm")
    >>> image == image2
    True
    
    >>> save_compressed_image([['8xx7'],['7xxx1']], "test2.pgm")
    Traceback (most recent call last):
    AssertionError: Each pixel must only contain one x.
    
    >>> save_compressed_image([['8x7'],['7x1']], "testing.pgm")
    Traceback (most recent call last):
    AssertionError: The sum of all b values in each row must be the same.
    """
    validate(nested_list,True)
    width = str(len(nested_list))
    b = sum_bs_row(nested_list[0])
    height = b
    image_format = "P2C"
    max_white_value = '255'
    file = open(file_name,'w')
    file.write(image_format+'\n')
    file.write(str(height) + " "+str(width) +'\n')
    file.write(max_white_value+'\n')   
    column = 0
   
    for row in range(len(nested_list)):
        for item in nested_list[row]:
            file.write(str(item))
            if column != len(nested_list[row]) -1:
                file.write(" ")
            column = column +1
        file.write("\n")
        column = 0
    file.close()
   


   
def save_image(nested_list,file_name):
    """ (list<list<>>,str) -> NoneType
        Saves the input list into the input file name based on the type of the values in the list.
        The list format is validated. If it is valid it is saved in an appropriate file (pgm or
        compressed pgm). If not, an AssertionError is raised.
        
        >>> image = [["0x5", "200x2","1x2"], ["111x7","2x2"]]
        >>> save_image(image, "test.pgm")
        >>> image2 = load_compressed_image("test.pgm")
        >>> image == image2
        True
        
        >>> image = [[0,1,255],[255,213,32]]
        >>> save_image(image, "test.pgm")
        >>> image2 = load_regular_image("test.pgm")
        >>> image == image2
        True
        
        >>> image = [[244,124,255,0,12],[255,213,32,12,14]]
        >>> save_image(image, "test.pgm")
        >>> image2 = load_regular_image("test.pgm")
        >>> image == image2
        True
        """
   
    if all_int(nested_list):
        validate(nested_list,False)
        save_regular_image(nested_list,file_name)
    elif all_str(nested_list):
        validate(nested_list,True)
        save_compressed_image(nested_list,file_name)
    else:
        raise AssertionError("The image matrix must contain only strings or only integers.")
   
def invert(image_matrix):
    """
    (list<list<int>>) -> (list<list<int>>)
    Returns an inverted version of the input image matrix
    by substracting each integer of the matrix from 255.
    >>> image = [[0, 100, 150], [200, 200, 200], [255, 255, 255]]
    >>> invert(image)
    [[255, 155, 105], [55, 55, 55], [0, 0, 0]]
    >>> image == [[0, 100, 150], [200, 200, 200], [255, 255, 255]]
    True
    
    >>> image = [[12, 145, 120,167], [233, 255, 120,190], [0, 255, 225,145]]
    >>> invert(image)
    [[243, 110, 135, 88], [22, 0, 135, 65], [255, 0, 30, 110]]
    
    >>> image = [[11, 135], [23, 215], [40, 215]]
    >>> invert(image)
    [[244, 120], [232, 40], [215, 40]]
    """
   
    validate(image_matrix,False)
    inverted_image = []
   
    for row in image_matrix:
        inverted_row = []
        for col in row:
            complement = 255 - col
            inverted_row.append(complement)
        inverted_image.append(inverted_row)
    return inverted_image


def flip_horizontal(image_matrix):
    """
    (list<list<int>>) -> (list<list<int>>)
    Returns a version of the input image matrix that is
    horizontally flipped by reversing the elements of each row.
    >>> image = [[1, 2, 3, 4, 5], [0, 0, 5, 10, 10], [5, 5, 5, 5, 5]]
    >>> flip_horizontal(image)
    [[5, 4, 3, 2, 1], [10, 10, 5, 0, 0], [5, 5, 5, 5, 5]]
    
    >>> image = [[0,14,144],[243,125,167]]
    >>> flip_horizontal(image)
    [[144, 14, 0], [167, 125, 243]]
    
    >>> image = [[1,2,54,234],[24,45,189,112]]
    >>> flip_horizontal(image)
    [[234, 54, 2, 1], [112, 189, 45, 24]]
    """
    validate(image_matrix,False)
    flipped_matrix = []
   
    for row in image_matrix:
        flipped_row = row[::-1]
        flipped_matrix.append(flipped_row)
    return flipped_matrix


def flip_vertical(image_matrix):
    """
    (list<list<int>>) -> (list<list<int>>)
    Returns a version of the input image matrix that is
    vertically flipped by reversing the elements of each column.
    >>> image = [[1, 2, 3, 4, 5], [0, 0, 5, 10, 10], [5, 5, 5, 5, 5]]
    >>> flip_vertical(image)
    [[5, 5, 5, 5, 5], [0, 0, 5, 10, 10], [1, 2, 3, 4, 5]]
    
    >>> image = [[0,1,2],[20,22,14],[90,13,43]]
    >>> flip_vertical(image)
    [[90, 13, 43], [20, 22, 14], [0, 1, 2]]
    >>> image = [[23,42],[12,45]]
    >>> flip_vertical(image)
    [[12, 45], [23, 42]]
    """
    validate(image_matrix,False)
    flipped_matrix = image_matrix[::-1]
   
    return flipped_matrix


def crop(image_matrix,top_left_row,top_left_column,n_row,n_column):
    """
    (list<list<int>>,int,int,int,int) -> list<list<int>>
    Returns a cropped image matrix corresponding
    to the target rectangle described by the input rows and columns.
    >>> crop([[5, 5, 5], [5, 6, 6], [6, 6, 7]], 1, 1, 2, 2)
    [[6, 6], [6, 7]]
    
    >>> crop([[4, 5, 6], [7, 8, 9], [10, 11, 12]], 0, 0, 1, 1)
    [[4]]
    
    >>> crop([[4, 5, 6], [7, 8, 9], [10, 11, 12]], 2, 2, 1, 1)
    [[12]]
    """
    validate(image_matrix,False)
    # crop rows
    start = top_left_row
    end = top_left_row + n_row
    cropped_matrix = image_matrix[start:end]
    # crop columns
    for i in range(len(cropped_matrix)):
        start = top_left_column
        end = top_left_column + n_column
        cropped_matrix[i] = cropped_matrix[i][start:end]
    return cropped_matrix


def is_last_target(int_list,target,column):
    """
    (list<int>,int,int) -> bool
    Determines if an integer is the last target in a list.
    Starts at the input column in the input list and
    checks if the previous or next columns contain the target
    to determine if it's the first target.
    >>> is_last_target([5, 3, 5, 5, 5, -1, 0], 5, 0)
    True
    >>> is_last_target([5, 3, 5, 5, 5, -1, 0], 5, 2)
    False
    >>> is_last_target([5, 3, 5, 5, 5, -1, 0], 5, 4)
    True
    """
    if column > 0 and column < len(int_list)-1:
        last_target_repeated = (int_list[column-1] == target) and (int_list[column+1] != target)
        last_target_not_repeated = (int_list[column-1] != target) and (int_list[column+1] != target)
        last_target = last_target_repeated or last_target_not_repeated
    
    if column == len(int_list)-1:
        last_target = True
    if column == 0:
        if len(int_list) > 1:
            last_target = int_list[column+1] != target
        else:
            last_target = True
    return last_target

def find_end_of_repetition(int_list,index,target):
    """
    (list<int>,int,int) -> int
    Finds and returns the index of the last consecutive occurence
    of the target input number. It starts looking after
    the input index.
    >>> find_end_of_repetition([5, 3, 5, 5, 5, -1, 0], 2, 5)
    4
    >>> find_end_of_repetition([1, 1, 1, 1, 4, 2, 6], 0, 1)
    3
    >>> find_end_of_repetition([1, 1, 1, 1, 4, 2, 6], 3, 1)
    3
    """
    last_occurence = index
    last_target = False
    column = index
    # Check bounds of the input index
    if column >= len(int_list) or column < 0:
        last_occurence = -1
    else:
        # Check that the target is indeed at the input index
        if int_list[column] == target:
            while( not last_target and column < len(int_list)):
                if int_list[column] == target:          
                    last_target = is_last_target(int_list,target,column)
                    if not last_target:
                        column = column+1
            last_occurence = column
        else:
            last_occurence = -1
        
    return last_occurence


def is_first_target(int_list,target,column):
    """
    (list<int>,int,int) -> bool
    Determines if an integer is the first target in a list.
    Starts at the input column in the input list and
    checks if the previous or next columns contain the target
    to determine if it's the first target.
    >>> is_first_target([5, 3, 5, 5, 5, -1, 0], 5, 0)
    True
    >>> is_first_target([5, 3, 5, 5, 5, -1, 0], 5, 2)
    True
    >>> is_first_target([5, 3, 5, 5, 5, -1, 0], 5, 4)
    False
    """
    if column > 0 and column < len(int_list)-1:
        first_target = (int_list[column-1] != target and (int_list[column+1] == target or int_list[column] == target))
    if column == len(int_list)-1:
        first_target = int_list[column-1] != target and int_list[column] == target
    if column == 0:
        if (len(int_list) > 1):
            first_target = int_list[column+1] == target or int_list[column] == target
        else:
            first_target = True
   
    return first_target

def compress(image_matrix):
    """
    (list<list<>>) -> list<list<str>>
    Compresses the non-compressed input image matrix
    and returns it.
    >>> compress([[11, 11, 11, 11, 11], [1, 5, 5, 5, 7], [255, 255, 255, 0, 255]])
    [['11x5'], ['1x1', '5x3', '7x1'], ['255x3', '0x1', '255x1']]
    >>> compress([[11,"abc"],["def"]])
    Traceback (most recent call last):
    AssertionError: The image matrix must contain only integers.
    >>> compress ([[270,3421],[-123,-234]])
    Traceback (most recent call last):
    AssertionError: Each element of the image matrix must be greater than 0 and less than 255.
    """
    validate(image_matrix,False)

    compressed_image = []
    column = 0
    for row in range(len(image_matrix)):
        new_row = []
        for column in range(len(image_matrix[row])):
            target = image_matrix[row][column]
            first_target = is_first_target(image_matrix[row],target,column)
            if(first_target):
                end = find_end_of_repetition(image_matrix[row],column,target)
                repetitions = (end - column)+1
                new_column = str(target) + "x"+str(repetitions)
                new_row.append(new_column)
               
        compressed_image.append(new_row)
       
    return compressed_image

def decompress(compressed_image_matrix):
    """
    (list<list<>>) -> (list<list<int>>)
    Decompresses the input image matrix
    and returns the decompressed matrix.
    >>> decompress([['11x5'], ['1x1', '5x3', '7x1'], ['255x3', '0x1', '255x1']])
    [[11, 11, 11, 11, 11], [1, 5, 5, 5, 7], [255, 255, 255, 0, 255]]
    >>> image = [[11, 11, 11, 11, 11], [1, 5, 5, 5, 7], [255, 255, 255, 0, 255]]
    >>> compressed_image = compress(image)
    >>> image2 = decompress(compressed_image)
    >>> image == image2
    True
    
    >>> decompress([["35x4"],["14x2","12x2"],["45x4"]])
    [[35, 35, 35, 35], [14, 14, 12, 12], [45, 45, 45, 45]]
    
    >>> decompress([["-341x4"],["-214x2","12x2"],["45x4"]])
    Traceback (most recent call last):
    AssertionError: The a integer must be between 0 and 255 and the b integer must be positive and greater than 0.
    """
    validate(compressed_image_matrix,True)
    decompressed_matrix = []
   
    for row in compressed_image_matrix:
        decompressed_row = []
        for column in row:
            x_index = column.find("x")
            pixel = column[0:x_index]
            pixel = int(pixel)
            repetitions = column[x_index+1:len(column)]
            repetitions = int(repetitions)
            for i in range(repetitions):
                decompressed_row.append(pixel)
        decompressed_matrix.append(decompressed_row)
    return decompressed_matrix

def validate_parameters(function,parameters):
    """
    (str,str) -> NoneType
    Validates the parameters of a function.
    
    >>> validate_parameters('CR',"")
    Traceback (most recent call last):
    AssertionError: The CR command must have 4 positive integer parameters.
    >>> validate_parameters('CP',"")
    
    >>> validate_parameters('FV',"")
    
    """
    if function in ['LOAD','SAVE']:
        error_message = "The "+function +"command requires a file name parameter."
        if parameters == "":
            raise AssertionError(error_message)
    if function in ['INV','FH','FV','DC','CP']:
        error_message = "The "+function +" command does not require parameters."
        if parameters != "":
            raise AssertionError(error_message)
        
    if function == 'CR':
        error_message = "The CR command must have 4 positive integer parameters."
        inputs = parameters.split(",")
        if len(inputs) != 4:
            raise AssertionError(error_message)
        else:
            for i in range(len(inputs)):
                if not inputs[i].isdecimal():
                    raise AssertionError(error_message)
    

    
def process_command(commands):
    """
    (str) -> NoneType
    Executes the image processing commands in the input string.
    The input string must be a series of space-separated commands.
    >>> process_command("LOAD<comp.pgm> CP DC INV INV SAVE<comp2.pgm>")
    
    >>> image = load_image("comp.pgm")
    >>> image2 = load_image("comp2.pgm")
    >>> image == image2
    True
    
    >>> process_command("LOAD<comp.pgm> FH INV SAVE<comp2.pgm>")
    >>> image = invert(flip_horizontal(load_image("comp.pgm")))
    >>> image2 = load_image("comp2.pgm")
    >>> image == image2
    True
    >>> process_command("LOAD<comp.pgm> FH CR<1,1,2,2> SAVE<comp2.pgm>")
    >>> image = crop(flip_horizontal(load_image("comp.pgm")),1,1,2,2)
    >>> image2 = load_image("comp2.pgm")
    >>> image == image2
    True
    """
    valid_commands = ['LOAD', 'SAVE', 'INV', 'FH', 'FV', 'CR', 'CP', 'DC']
    list_commands = commands.split()
    parameters = ""
    image_matrix = []
    extra_chars = ""
    for command in list_commands:
        if command.find("<") != -1 and command.find(">") != -1:
            function = command.split("<")[0]
            end_parameters = command.split("<")[1].find(">")
            if len(command.split("<")[1])> end_parameters+1:
                extra_chars = command.split("<")[1][end_parameters+1:]

            if extra_chars != "":
                raise AssertionError("Extra characters are present in the "+function+" command.")
            
            parameters = command.split("<")[1].replace(">","")
        else:
            function = command
            parameters = ""

        if function not in valid_commands:
            raise AssertionError("An invalid command is present in the input string")

        validate_parameters(function,parameters)
        image_matrix = execute_function(image_matrix,function,parameters)
        
def execute_function(image_matrix,function,parameters):
    """
    (list<list<>>,str,str) -> list<list<>>
    Executes the input image processing function
    with the given input parameters.
    
    >>> image = [[1,2,3,4],[5,6,7,8]]
    >>> image1 = execute_function(image,'FV',None)
    >>> image2 = flip_vertical(image)
    >>> image1 == image2
    True
    >>> image = [[1,2,3,4],[5,6,7,8]]
    >>> image1 = execute_function(image,'FH',None)
    >>> image2 = flip_horizontal(image)
    >>> image1 == image2
    True
    >>> image = [[1,2,3,4],[5,6,7,8]]
    >>> image1 = execute_function(image,'CP',None)
    >>> image2 = compress(image)
    >>> image1 == image2
    True
    
    """

    if function == 'LOAD':
        image_matrix = load_image(parameters)
    if function == 'SAVE':
        save_image(image_matrix,parameters)
    if function == 'INV':
        image_matrix = invert(image_matrix)
    if function == 'FH':
        image_matrix = flip_horizontal(image_matrix)
    if function == 'FV':
        image_matrix = flip_vertical(image_matrix)
    if function == 'CR':
        inputs = parameters.split(",")
        top_left_row = int(inputs[0])
        top_left_column = int(inputs[1])
        n_row = int(inputs[2])
        n_col = int(inputs[3])
        image_matrix = crop(image_matrix,top_left_row,top_left_column,n_row,n_col)
    if function == 'CP':
        image_matrix = compress(image_matrix)
    if function == 'DC':
        image_matrix = decompress(image_matrix)
    return image_matrix

