Image processor written in Python that performs various operations on PGM format images such as flipping,
cropping, inverting, reading from disk,writing to disk, compressing and decompressing.
